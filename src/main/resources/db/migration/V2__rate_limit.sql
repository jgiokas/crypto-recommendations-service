CREATE TABLE IF NOT EXISTS ip_rate_limit
(
    ip_address VARCHAR(64) PRIMARY KEY,
    state BYTEA
);