CREATE TABLE IF NOT EXISTS btc
(
    id
    BIGSERIAL
    PRIMARY
    KEY,
    price
    NUMERIC
    NOT
    NULL,
    timestamp
    TIMESTAMP
    NOT
    NULL
);

CREATE INDEX IF NOT EXISTS idx_btc_price ON btc (price);
CREATE INDEX IF NOT EXISTS idx_btc_timestamp ON btc (timestamp);

CREATE TABLE IF NOT EXISTS eth
(
    id
    BIGSERIAL
    PRIMARY
    KEY,
    price
    NUMERIC
    NOT
    NULL,
    timestamp
    TIMESTAMP
    NOT
    NULL
);

CREATE INDEX IF NOT EXISTS idx_eth_price ON eth (price);
CREATE INDEX IF NOT EXISTS idx_eth_timestamp ON eth (timestamp);

CREATE TABLE IF NOT EXISTS doge
(
    id
    BIGSERIAL
    PRIMARY
    KEY,
    price
    NUMERIC
    NOT
    NULL,
    timestamp
    TIMESTAMP
    NOT
    NULL
);

CREATE INDEX IF NOT EXISTS idx_doge_price ON doge (price);
CREATE INDEX IF NOT EXISTS idx_doge_timestamp ON doge (timestamp);

CREATE TABLE IF NOT EXISTS ltc
(
    id
    BIGSERIAL
    PRIMARY
    KEY,
    price
    NUMERIC
    NOT
    NULL,
    timestamp
    TIMESTAMP
    NOT
    NULL
);

CREATE INDEX IF NOT EXISTS idx_ltc_price ON ltc (price);
CREATE INDEX IF NOT EXISTS idx_ltc_timestamp ON ltc (timestamp);

CREATE TABLE IF NOT EXISTS xrp
(
    id
    BIGSERIAL
    PRIMARY
    KEY,
    price
    NUMERIC
    NOT
    NULL,
    timestamp
    TIMESTAMP
    NOT
    NULL
);

CREATE INDEX IF NOT EXISTS idx_xrp_price ON xrp (price);
CREATE INDEX IF NOT EXISTS idx_xrp_timestamp ON xrp (timestamp);
