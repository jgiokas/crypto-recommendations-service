package com.xm.crs.ratelimit;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Component
public class RateLimiterInterceptor extends HandlerInterceptorAdapter {

    private static final int RATE_LIMIT = 10;
    private static final int BUCKET_SIZE = 10;
    private static final long TOKEN_INTERVAL = 60 * 1000 / RATE_LIMIT;

    private final StringRedisTemplate redisTemplate;

    public RateLimiterInterceptor(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final var ip = request.getRemoteAddr();

        final var ops = redisTemplate.opsForValue();

        final var tokenCountKey = "rate_limit:tokens:" + ip;
        final var lastRefillKey = "rate_limit:last_refill:" + ip;

        final var tokenCountValue = ops.get(tokenCountKey);
        final var lastRefillValue = ops.get(lastRefillKey);

        final var now = Instant.now().toEpochMilli();

        final var tokenCount = tokenCountValue != null ? Long.parseLong(tokenCountValue) : BUCKET_SIZE;
        final var lastRefill = lastRefillValue != null ? Long.parseLong(lastRefillValue) : now;

        final var tokensToAdd = (now - lastRefill) / TOKEN_INTERVAL;
        final var newTokenCount = Math.min(tokenCount + tokensToAdd, BUCKET_SIZE);

        if (newTokenCount < 1) {
            response.setStatus(429);
            response.getWriter().write("Too Many Requests");
            return false;
        } else {
            response.setHeader("X-Rate-Limit-Remaining", String.valueOf(newTokenCount - 1));
            ops.set(tokenCountKey, String.valueOf(newTokenCount - 1), 1, TimeUnit.HOURS);
            ops.set(lastRefillKey, String.valueOf(now), 1, TimeUnit.HOURS);
            return true;
        }
    }
}