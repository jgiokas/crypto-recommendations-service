package com.xm.crs.application;

import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.NormalizedRange;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.reverseOrder;

@Service
public class GetAllNormalizedRanges {

    private final CryptoRecommendationRepository recommendationRepository;

    private static final Cryptocurrency[] CRYPTOCURRENCIES = Cryptocurrency.values();

    public GetAllNormalizedRanges(CryptoRecommendationRepository recommendationRepository) {
        this.recommendationRepository = recommendationRepository;
    }

    public List<NormalizedRange> execute() {
        List<NormalizedRange> normalizedRanges = new ArrayList<>(CRYPTOCURRENCIES.length);

        for (final Cryptocurrency symbol : CRYPTOCURRENCIES) {
            final var summary = recommendationRepository.fetchSummary(symbol);
            final var minPrice = summary.minPrice();
            final var maxPrice = summary.maxPrice();

            final var normalizedRange = maxPrice.subtract(minPrice).divide(minPrice,2, RoundingMode.HALF_UP);

            normalizedRanges.add(new NormalizedRange(symbol, normalizedRange));
        }

        normalizedRanges.sort(reverseOrder());
        return normalizedRanges;
    }
}