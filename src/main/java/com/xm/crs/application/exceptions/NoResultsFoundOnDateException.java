package com.xm.crs.application.exceptions;

public class NoResultsFoundOnDateException extends RuntimeException {

    public NoResultsFoundOnDateException(String message) {
        super(message);
    }
}
