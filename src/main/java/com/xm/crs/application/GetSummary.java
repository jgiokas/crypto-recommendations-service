package com.xm.crs.application;

import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.Summary;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.springframework.stereotype.Service;

@Service
public class GetSummary {

    private final CryptoRecommendationRepository recommendationRepository;

    public GetSummary(CryptoRecommendationRepository recommendationRepository) {
        this.recommendationRepository = recommendationRepository;
    }

    public Summary execute(Cryptocurrency symbol) {
        return recommendationRepository.fetchSummary(symbol);
    }
}
