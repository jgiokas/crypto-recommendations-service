package com.xm.crs.application;

import com.xm.crs.application.exceptions.NoResultsFoundOnDateException;
import com.xm.crs.core.NormalizedRange;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Comparator;

@Service
public class GetHighestNormalizedSymbol {

    private final CryptoRecommendationRepository cryptoRecommendationRepository;

    public GetHighestNormalizedSymbol(CryptoRecommendationRepository cryptoRecommendationRepository) {
        this.cryptoRecommendationRepository = cryptoRecommendationRepository;
    }

    public NormalizedRange execute(LocalDate date) {
        return cryptoRecommendationRepository.fetchMinMax(date).stream()
                .map(e ->
                    new NormalizedRange(
                            e.symbol(),
                            e.maxPrice().subtract(e.minPrice()).divide(e.minPrice(), 2, RoundingMode.HALF_UP)))
                .max(Comparator.comparing(NormalizedRange::normalizedRange))
                .orElseThrow(() -> new NoResultsFoundOnDateException("No results found in given date: " + date));

    }
}
