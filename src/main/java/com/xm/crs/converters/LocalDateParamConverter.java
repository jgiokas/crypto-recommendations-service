package com.xm.crs.converters;

import com.xm.crs.converters.exceptions.InvalidLocalDateParameterException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.DateTimeException;
import java.time.LocalDate;

@Component
public class LocalDateParamConverter implements Converter<String, LocalDate> {
    @Override
    public LocalDate convert(String source) {
        try {
            return LocalDate.parse(source);
        } catch (DateTimeException | NullPointerException e) {
            throw new InvalidLocalDateParameterException("Invalid date: " + source);
        }
    }
}
