package com.xm.crs.converters.exceptions;

public class InvalidCryptocurrencyParameterException extends RuntimeException {

    public InvalidCryptocurrencyParameterException(String message) {
        super(message);
    }
}
