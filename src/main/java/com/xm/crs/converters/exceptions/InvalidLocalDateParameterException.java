package com.xm.crs.converters.exceptions;

public class InvalidLocalDateParameterException extends RuntimeException {

    public InvalidLocalDateParameterException(String message) {
        super(message);
    }
}
