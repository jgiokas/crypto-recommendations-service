package com.xm.crs.converters;

import com.xm.crs.converters.exceptions.InvalidCryptocurrencyParameterException;
import com.xm.crs.core.Cryptocurrency;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CryptocurrencyConverter implements Converter<String, Cryptocurrency> {

    @Override
    public Cryptocurrency convert(String source) {
        try {
            return Cryptocurrency.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidCryptocurrencyParameterException("Invalid or unsupported cryptocurrency: " + source);
        }
    }
}
