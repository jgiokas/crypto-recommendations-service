package com.xm.crs.rest;

import org.springframework.http.HttpStatus;

import java.time.Instant;

public record ErrorResponse(
        String error,
        int status,
        Instant timestamp
) {
    public static ErrorResponse badRequest(String error) {
        return new ErrorResponse(error, HttpStatus.BAD_REQUEST.value(), Instant.now());
    }

    public static ErrorResponse notFound(String error) {
        return new ErrorResponse(error, HttpStatus.NOT_FOUND.value(), Instant.now());
    }
}
