package com.xm.crs.rest;

import com.xm.crs.application.GetAllNormalizedRanges;
import com.xm.crs.application.GetHighestNormalizedSymbol;
import com.xm.crs.application.GetSummary;
import com.xm.crs.application.exceptions.NoResultsFoundOnDateException;
import com.xm.crs.converters.exceptions.InvalidCryptocurrencyParameterException;
import com.xm.crs.converters.exceptions.InvalidLocalDateParameterException;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.NormalizedRange;
import com.xm.crs.core.Summary;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@OpenAPIDefinition(
        info = @Info(
                title = "Crypto Recommendation Service",
                version = "v1"
        )
)
@RestController
@RequestMapping("/v1/cryptos")
@Tag(
        name = "Crypto Recommendations",
        description = "Retrieve information about cryptos"
)
public class CryptoRecommendationController {

    private final GetSummary getSummary;
    private final GetAllNormalizedRanges getAllNormalizedRanges;
    private final GetHighestNormalizedSymbol getHighestNormalizedSymbol;

    public CryptoRecommendationController(
            GetSummary getSummary,
            GetAllNormalizedRanges getAllNormalizedRanges,
            GetHighestNormalizedSymbol getHighestNormalizedSymbol
    ) {
        this.getSummary = getSummary;
        this.getAllNormalizedRanges = getAllNormalizedRanges;
        this.getHighestNormalizedSymbol = getHighestNormalizedSymbol;
    }

    @Operation(summary = "Returns a list in descending order of all supported cryptos comparing the normalized range")
    @GetMapping("/normalized")
    public List<NormalizedRange> getNormalizedRange() {
        return getAllNormalizedRanges.execute();
    }

    @Operation(summary = "Returns a summary containing min/max/earliest/latest price of the given crypto")
    @GetMapping("/{crypto}")
    public Summary getSummary(@PathVariable("crypto") Cryptocurrency cryptocurrency) {
        return getSummary.execute(cryptocurrency);
    }

    @Operation(summary = "Returns a summary containing min/max/earliest/latest price of the given crypto")
    @GetMapping("/normalized/highest")
    public NormalizedRange getHighestNormalizedRange(@RequestParam("date") LocalDate date) {
        return getHighestNormalizedSymbol.execute(date);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMissingParams(MissingServletRequestParameterException ex) {
        return ErrorResponse.badRequest("Parameter " + ex.getParameterName() + " is missing");
    }

    @ExceptionHandler(InvalidLocalDateParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidLocalDateParameterException(InvalidLocalDateParameterException ex) {
        return ErrorResponse.badRequest(ex.getMessage());
    }

    @ExceptionHandler(InvalidCryptocurrencyParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidCryptocurrencyParameterException(InvalidCryptocurrencyParameterException ex) {
        return ErrorResponse.badRequest(ex.getMessage());
    }

    @ExceptionHandler(NoResultsFoundOnDateException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNoResultsFoundOnDateException(NoResultsFoundOnDateException ex) {
        return ErrorResponse.notFound(ex.getMessage());
    }
}
