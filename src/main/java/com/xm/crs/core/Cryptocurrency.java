package com.xm.crs.core;

public enum Cryptocurrency {
    BTC,
    ETH,
    LTC,
    XRP,
    DOGE;
}
