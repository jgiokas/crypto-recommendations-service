package com.xm.crs.core;

import java.math.BigDecimal;

public record MinMax(
        Cryptocurrency symbol,
        BigDecimal minPrice,
        BigDecimal maxPrice
) {
}
