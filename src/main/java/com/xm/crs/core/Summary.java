package com.xm.crs.core;

import java.math.BigDecimal;

public record Summary(
        BigDecimal minPrice,
        BigDecimal maxPrice,
        BigDecimal earliestPrice,
        BigDecimal latestPrice
) {
}
