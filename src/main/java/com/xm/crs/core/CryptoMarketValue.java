package com.xm.crs.core;

import java.math.BigDecimal;
import java.time.Instant;

public record CryptoMarketValue(
        Instant timestamp,
        Cryptocurrency symbol,
        BigDecimal price
) {
    private static final int TIMESTAMP = 0;
    private static final int SYMBOL = 1;
    private static final int PRICE = 2;

    public static CryptoMarketValue parseCsv(final String csv) {
        String[] tokens = csv.split(",");

        final var timestamp = Instant.ofEpochMilli(Long.parseLong(tokens[TIMESTAMP]));
        final var symbol = Cryptocurrency.valueOf(tokens[SYMBOL]);
        final var price = new BigDecimal(tokens[PRICE]);

        return new CryptoMarketValue(timestamp, symbol, price);
    }
}
