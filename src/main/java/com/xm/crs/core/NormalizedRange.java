package com.xm.crs.core;

import java.math.BigDecimal;

public record NormalizedRange(Cryptocurrency symbol, BigDecimal normalizedRange) implements Comparable<NormalizedRange> {

    @Override
    public int compareTo(NormalizedRange o) {
        return normalizedRange.compareTo(o.normalizedRange);
    }
}
