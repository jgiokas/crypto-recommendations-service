package com.xm.crs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoRecommendationServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(CryptoRecommendationServiceApp.class, args);
    }
}
