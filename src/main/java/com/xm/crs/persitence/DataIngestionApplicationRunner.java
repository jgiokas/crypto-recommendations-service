package com.xm.crs.persitence;

import com.xm.crs.core.CryptoMarketValue;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Component
@Profile("!test")
public class DataIngestionApplicationRunner implements ApplicationRunner {

    private final CryptoRecommendationRepository cryptoRecommendationRepository;

    public DataIngestionApplicationRunner(CryptoRecommendationRepository cryptoRecommendationRepository) {
        this.cryptoRecommendationRepository = cryptoRecommendationRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws IOException {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("classpath:/data/*.csv");

        for (Resource resource : resources) {
            final var file = resource.getFile();

            try (FileReader fr = new FileReader(file);
                 BufferedReader br = new BufferedReader(fr)) {
                final var cryptoMarketValues = br.lines()
                        .skip(1)
                        .map(CryptoMarketValue::parseCsv)
                        .toList();

                cryptoRecommendationRepository.saveAll(cryptoMarketValues);
            }
        }
    }
}
