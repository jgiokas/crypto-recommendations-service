package com.xm.crs.persitence;

import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.MinMax;
import com.xm.crs.core.Summary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
class JdbcCryptoRecommendationRepository implements CryptoRecommendationRepository {

    private static final String SUMMARY_TEMPLATE_SQL = """
            SELECT
                (SELECT MIN(price) FROM %s) AS min_price,
                (SELECT MAX(price) FROM %s) AS max_price,
                (SELECT price FROM %s ORDER BY timestamp ASC LIMIT 1) AS oldest_price,
                (SELECT price FROM %s ORDER BY timestamp DESC LIMIT 1) AS newest_price;
            """;
    private static final String SAVE_TEMPLATE_SQL = """
            INSERT INTO %s (price, timestamp) VALUES (:price, :timestamp)""";
    private static final String MIN_MAX_TEMPLATE_SQL = initializeMinMaxSqlTemplate();
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcCryptoRecommendationRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static String tableFromSymbol(final Cryptocurrency symbol) {
        return symbol.name().toLowerCase();
    }

    @Override
    public Summary fetchSummary(Cryptocurrency symbol) {
        final var table = tableFromSymbol(symbol);
        final var sql = String.format(SUMMARY_TEMPLATE_SQL, table, table, table, table);

        return jdbcTemplate.query(sql, rs -> {
            rs.next();
            return new Summary(
                    rs.getBigDecimal("min_price"),
                    rs.getBigDecimal("max_price"),
                    rs.getBigDecimal("oldest_price"),
                    rs.getBigDecimal("newest_price"));
        });
    }

    @Override
    public List<MinMax> fetchMinMax(LocalDate date) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("timestamp", Date.valueOf(date));

        return jdbcTemplate.query(MIN_MAX_TEMPLATE_SQL, params, JdbcCryptoRecommendationRepository::extractFetchMinMaxResult);
    }

    private static List<MinMax> extractFetchMinMaxResult(ResultSet rs) throws SQLException {
        rs.next();
        List<MinMax> results = new ArrayList<>(Cryptocurrency.values().length);

        for (Cryptocurrency symbol : Cryptocurrency.values()) {
            final var normalizedSymbol = tableFromSymbol(symbol);
            final var minPrice = rs.getBigDecimal(String.format("%s_min_price", normalizedSymbol));
            final var maxPrice = rs.getBigDecimal(String.format("%s_max_price", normalizedSymbol));

            if (minPrice == null || maxPrice == null) {
                continue;
            }
            results.add(new MinMax(symbol, minPrice, maxPrice));
        }

        return results;
    }

    @Override
    public void save(CryptoMarketValue cryptoMarketValue) {
        final var table = cryptoMarketValue.symbol().name().toLowerCase();
        final var query = String.format(SAVE_TEMPLATE_SQL, table);
        jdbcTemplate.update(query, buildParams(cryptoMarketValue));
    }

    @Override
    public void saveAll(List<CryptoMarketValue> cryptoMarketValues) {
        final var cryptoMarketValuesBySymbol = cryptoMarketValues.stream().collect(Collectors.groupingBy(CryptoMarketValue::symbol));

        cryptoMarketValuesBySymbol.forEach((key, value) -> {
            final var table = tableFromSymbol(key);
            final var query = String.format(SAVE_TEMPLATE_SQL, table);
            jdbcTemplate.batchUpdate(query, buildParamsBatch(value));
        });
    }

    private SqlParameterSource buildParams(final CryptoMarketValue cryptoMarketValue) {
        return new MapSqlParameterSource()
                .addValue("price", cryptoMarketValue.price())
                .addValue("timestamp", Date.from(cryptoMarketValue.timestamp()));
    }

    private SqlParameterSource[] buildParamsBatch(final List<CryptoMarketValue> cryptoMarketValues) {
        return cryptoMarketValues.stream().map(this::buildParams).toArray(SqlParameterSource[]::new);
    }

    private static String initializeMinMaxSqlTemplate() {
        StringBuilder sb = new StringBuilder("SELECT\n");
        for (int i = 0; i < Cryptocurrency.values().length; i++) {
            final var symbol = tableFromSymbol(Cryptocurrency.values()[i]);
            final var minPriceAlias = String.format("%s_min_price", symbol);
            final var maxPriceAlias = String.format("%s_max_price", symbol);

            sb.append(String.format("\t(SELECT MIN(price) FROM %s WHERE DATE_TRUNC('DAY', timestamp) = :timestamp) AS %s,\n", symbol, minPriceAlias));
            sb.append(String.format("\t(SELECT MAX(price) FROM %s WHERE DATE_TRUNC('DAY', timestamp) = :timestamp) AS %s", symbol, maxPriceAlias));
            if (i == Cryptocurrency.values().length - 1) {
                sb.append("\n");
            } else {
                sb.append(",\n");
            }
        }
        return sb.toString();
    }
}
