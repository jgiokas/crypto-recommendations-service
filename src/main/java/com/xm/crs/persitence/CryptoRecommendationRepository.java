package com.xm.crs.persitence;

import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.MinMax;
import com.xm.crs.core.Summary;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CryptoRecommendationRepository {

    List<MinMax> fetchMinMax(LocalDate date);

    Summary fetchSummary(Cryptocurrency symbol);

    void save(CryptoMarketValue cryptoMarketValue);

    void saveAll(List<CryptoMarketValue> cryptoMarketValues);

}
