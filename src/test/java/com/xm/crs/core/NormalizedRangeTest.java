package com.xm.crs.core;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class NormalizedRangeTest {
    @Test
    void normalizedRangeCompareToShouldReturnCorrectValue() {
        // given
        final var higher = new NormalizedRange(Cryptocurrency.BTC, new BigDecimal("0.30"));
        final var lower = new NormalizedRange(Cryptocurrency.BTC, new BigDecimal("0.12"));
        final var equal = new NormalizedRange(Cryptocurrency.BTC, new BigDecimal("0.50"));
        final var equalOther = new NormalizedRange(Cryptocurrency.BTC, new BigDecimal("0.50"));

        // when/then
        assertThat(higher.compareTo(lower)).isPositive();
        assertThat(lower.compareTo(higher)).isNegative();
        assertThat(equal.compareTo(equalOther)).isZero();

    }
}