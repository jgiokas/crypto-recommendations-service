package com.xm.crs.core;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public class CryptoMarketValueTest {

    @Test
    void parseCsvShouldReturnCryptoMarketValueInstance() {
        // given
        final var csv = "1641308400000,BTC,62000.55";

        // when
        final var cryptoMarketValue = CryptoMarketValue.parseCsv(csv);

        // then
        assertThat(cryptoMarketValue.price()).isEqualTo(new BigDecimal("62000.55"));
        assertThat(cryptoMarketValue.timestamp()).isEqualTo(Instant.ofEpochMilli(1641308400000L));
        assertThat(cryptoMarketValue.symbol()).isEqualTo(Cryptocurrency.BTC);
    }
}
