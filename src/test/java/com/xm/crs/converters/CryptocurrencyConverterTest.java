package com.xm.crs.converters;

import com.xm.crs.BaseTest;
import com.xm.crs.converters.exceptions.InvalidCryptocurrencyParameterException;
import com.xm.crs.core.Cryptocurrency;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

public class CryptocurrencyConverterTest extends BaseTest {

    @Autowired
    private CryptocurrencyConverter cryptocurrencyConverter;

    @Test
    void givenInvalidCryptocurrencyValueThrowsException() {
        // given
        final var cryptocurrency = "xxx";

        // when/then
        assertThatThrownBy(() -> cryptocurrencyConverter.convert(cryptocurrency))
                .isInstanceOf(InvalidCryptocurrencyParameterException.class)
                .hasMessageContaining("Invalid or unsupported cryptocurrency: " + cryptocurrency);
    }

    @Test
    void givenValidCryptocurrencyValueConverts() {
        // given
        final var cryptocurrency = "BTC";

        // when/then
        assertThat(cryptocurrencyConverter.convert(cryptocurrency)).isEqualTo(Cryptocurrency.BTC);
    }
}
