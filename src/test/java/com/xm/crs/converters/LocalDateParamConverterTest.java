package com.xm.crs.converters;

import com.xm.crs.BaseTest;
import com.xm.crs.converters.exceptions.InvalidCryptocurrencyParameterException;
import com.xm.crs.converters.exceptions.InvalidLocalDateParameterException;
import com.xm.crs.core.Cryptocurrency;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

public class LocalDateParamConverterTest extends BaseTest {
    @Autowired
    private LocalDateParamConverter localDateParamConverter;

    @Test
    void givenInvalidDateValueThrowsException() {
        // given
        final var date = "xxx";

        // when/then
        assertThatThrownBy(() -> localDateParamConverter.convert(date))
                .isInstanceOf(InvalidLocalDateParameterException.class)
                .hasMessageContaining("Invalid date: " + date);
    }

    @Test
    void givenValidDateValueConverts() {
        // given
        final var date = "2023-03-28";

        // when/then
        assertThat(localDateParamConverter.convert(date)).isEqualTo(LocalDate.parse(date));
    }
}
