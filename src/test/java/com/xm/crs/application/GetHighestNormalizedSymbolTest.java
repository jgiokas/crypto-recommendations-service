package com.xm.crs.application;

import com.xm.crs.BaseTest;
import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.NormalizedRange;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GetHighestNormalizedSymbolTest extends BaseTest {

    @Autowired
    GetHighestNormalizedSymbol getHighestNormalizedRangeTest;

    @Autowired
    CryptoRecommendationRepository cryptoRecommendationRepository;

    @AfterEach
    void cleanup() {
        jdbcTestDatabaseRepository.truncate();
    }

    @Test
    void testGetHighestNormalizedSymbolTest() {
        // given
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(todayInstant, Cryptocurrency.BTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.BTC, new BigDecimal("2.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.ETH, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.ETH, new BigDecimal("3.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.XRP, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.XRP, new BigDecimal("4.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.LTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.LTC, new BigDecimal("5.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.DOGE, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.DOGE, new BigDecimal("6.00"))
                )
        );

        final var normalizedRange = getHighestNormalizedRangeTest.execute(LocalDate.ofInstant(todayInstant, ZoneOffset.UTC));

        assertThat(normalizedRange.symbol()).isEqualTo(Cryptocurrency.DOGE);
        assertThat(normalizedRange.normalizedRange()).isEqualTo(new BigDecimal("5.00"));
    }
}
