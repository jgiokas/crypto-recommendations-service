package com.xm.crs.application;

import com.xm.crs.BaseTest;
import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.core.NormalizedRange;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GetAllNormalizedRangesTest extends BaseTest {

    @Autowired
    GetAllNormalizedRanges getAllNormalizedRanges;

    @Autowired
    CryptoRecommendationRepository cryptoRecommendationRepository;

    @AfterEach
    void cleanup() {
        jdbcTestDatabaseRepository.truncate();
    }

    @Test
    void testGetHighestNormalizedSymbolTest() {
        // given
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(todayInstant, Cryptocurrency.BTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.BTC, new BigDecimal("2.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.BTC, new BigDecimal("3.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.ETH, new BigDecimal("4.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.ETH, new BigDecimal("5.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.ETH, new BigDecimal("7.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.XRP, new BigDecimal("8.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.XRP, new BigDecimal("9.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.XRP, new BigDecimal("12.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.LTC, new BigDecimal("13.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.LTC, new BigDecimal("14.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.LTC, new BigDecimal("18.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.DOGE, new BigDecimal("19.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.DOGE, new BigDecimal("20.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.DOGE, new BigDecimal("25.00"))
                )
        );

        final var normalizedRange = getAllNormalizedRanges.execute();

        assertThat(normalizedRange).hasSize(5);
        assertThat(normalizedRange).hasSameElementsAs(List.of(
                new NormalizedRange(Cryptocurrency.BTC, new BigDecimal("2.00")),
                new NormalizedRange(Cryptocurrency.ETH, new BigDecimal("0.75")),
                new NormalizedRange(Cryptocurrency.XRP, new BigDecimal("0.50")),
                new NormalizedRange(Cryptocurrency.LTC, new BigDecimal("0.38")),
                new NormalizedRange(Cryptocurrency.DOGE, new BigDecimal("0.32"))
        ));
    }
}