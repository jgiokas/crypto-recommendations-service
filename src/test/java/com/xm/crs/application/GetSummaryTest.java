package com.xm.crs.application;

import com.xm.crs.BaseTest;
import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GetSummaryTest extends BaseTest {

    @Autowired
    GetSummary getSummary;

    @Autowired
    CryptoRecommendationRepository cryptoRecommendationRepository;

    @AfterEach
    void cleanup() {
        jdbcTestDatabaseRepository.truncate();
    }

    @Test
    void testGetSummary() {
        // given
        final var expectedMinPrice = new BigDecimal("12345.67");
        final var expectedMaxPrice = new BigDecimal("98765.43");
        final var expectedEarliestPrice = new BigDecimal("23456.78");
        final var expectedLatestPrice = new BigDecimal("34567.89");
        final var otherPrice = new BigDecimal("45678.9");

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(Instant.now(), Cryptocurrency.BTC, expectedEarliestPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(1)), Cryptocurrency.BTC, expectedMinPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(2)), Cryptocurrency.BTC, expectedMaxPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(3)), Cryptocurrency.BTC, otherPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(4)), Cryptocurrency.BTC, expectedLatestPrice),
                        new CryptoMarketValue(Instant.now(), Cryptocurrency.ETH, expectedEarliestPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(1)), Cryptocurrency.ETH, expectedMinPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(2)), Cryptocurrency.ETH, expectedMaxPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(3)), Cryptocurrency.ETH, otherPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(4)), Cryptocurrency.ETH, expectedLatestPrice)
                )
        );

        // when
        final var btcSummary = cryptoRecommendationRepository.fetchSummary(Cryptocurrency.BTC);
        final var ethSummary = cryptoRecommendationRepository.fetchSummary(Cryptocurrency.ETH);

        // then
        assertThat(btcSummary.earliestPrice()).isEqualTo(expectedEarliestPrice);
        assertThat(btcSummary.latestPrice()).isEqualTo(expectedLatestPrice);
        assertThat(btcSummary.minPrice()).isEqualTo(expectedMinPrice);
        assertThat(btcSummary.maxPrice()).isEqualTo(expectedMaxPrice);

        assertThat(ethSummary.earliestPrice()).isEqualTo(expectedEarliestPrice);
        assertThat(ethSummary.latestPrice()).isEqualTo(expectedLatestPrice);
        assertThat(ethSummary.minPrice()).isEqualTo(expectedMinPrice);
        assertThat(ethSummary.maxPrice()).isEqualTo(expectedMaxPrice);
    }
}
