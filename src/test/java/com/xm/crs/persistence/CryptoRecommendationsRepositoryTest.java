package com.xm.crs.persistence;

import com.xm.crs.BaseTest;
import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CryptoRecommendationsRepositoryTest extends BaseTest {

    @Autowired
    CryptoRecommendationRepository cryptoRecommendationRepository;

    @AfterEach
    void cleanup() {
        jdbcTestDatabaseRepository.truncate();
    }

    @Test
    void givenSingleEntryReturnSummaryOfGivenSymbol() {
        // given
        final var expectedPrice = new BigDecimal("12345.67");
        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(Instant.now(), Cryptocurrency.BTC, expectedPrice)
                )
        );

        // when
        final var summary = cryptoRecommendationRepository.fetchSummary(Cryptocurrency.BTC);

        // then
        assertThat(summary.earliestPrice()).isEqualTo(expectedPrice);
        assertThat(summary.latestPrice()).isEqualTo(expectedPrice);
        assertThat(summary.minPrice()).isEqualTo(expectedPrice);
        assertThat(summary.maxPrice()).isEqualTo(expectedPrice);
    }

    @Test
    void givenMultipleEntriesReturnSummaryOfGivenSymbol() {
        // given
        final var expectedMinPrice = new BigDecimal("12345.67");
        final var expectedMaxPrice = new BigDecimal("98765.43");
        final var expectedEarliestPrice = new BigDecimal("23456.78");
        final var expectedLatestPrice = new BigDecimal("34567.89");
        final var otherPrice = new BigDecimal("45678.9");

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(Instant.now(), Cryptocurrency.BTC, expectedEarliestPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(1)), Cryptocurrency.BTC, expectedMinPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(2)), Cryptocurrency.BTC, expectedMaxPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(3)), Cryptocurrency.BTC, otherPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(4)), Cryptocurrency.BTC, expectedLatestPrice)
                )
        );

        // when
        final var summary = cryptoRecommendationRepository.fetchSummary(Cryptocurrency.BTC);

        // then
        assertThat(summary.earliestPrice()).isEqualTo(expectedEarliestPrice);
        assertThat(summary.latestPrice()).isEqualTo(expectedLatestPrice);
        assertThat(summary.minPrice()).isEqualTo(expectedMinPrice);
        assertThat(summary.maxPrice()).isEqualTo(expectedMaxPrice);
    }

    @Test
    void givenMultipleEntriesReturnMinMaxOfGivenDate() {
        // given
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);
        final var expectedMinPrice = new BigDecimal("12345.67");
        final var expectedMaxPrice = new BigDecimal("98765.43");
        final var otherPrice = new BigDecimal("23456.78");
        final var otherDayPrice = new BigDecimal("45678.9");


        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(todayInstant, Cryptocurrency.BTC, expectedMaxPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.BTC, expectedMinPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.BTC, otherPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(2)), Cryptocurrency.BTC, otherDayPrice),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.ETH, expectedMaxPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.ETH, expectedMinPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.ETH, otherPrice),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(2)), Cryptocurrency.ETH, otherDayPrice)
                )
        );

        // when
        final var minMaxes = cryptoRecommendationRepository.fetchMinMax(LocalDate.ofInstant(todayInstant, ZoneOffset.UTC));

        // then
        assertThat(minMaxes).hasSize(2);
        assertThat(minMaxes.get(0).minPrice()).isEqualTo(expectedMinPrice);
        assertThat(minMaxes.get(0).maxPrice()).isEqualTo(expectedMaxPrice);
        assertThat(minMaxes.get(1).minPrice()).isEqualTo(expectedMinPrice);
        assertThat(minMaxes.get(1).maxPrice()).isEqualTo(expectedMaxPrice);
    }
}
