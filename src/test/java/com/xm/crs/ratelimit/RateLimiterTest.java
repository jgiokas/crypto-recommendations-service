package com.xm.crs.ratelimit;

import com.xm.crs.BaseTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class RateLimiterTest extends BaseTest {

    @Test
    @Disabled
    void testRateLimitIsApplied() {
        var attemptsLeft = 10L;

        for (int i = 0; i < 10; i++) {
            webTestClient.get().uri(uriBuilder -> uriBuilder
                            .path("/v1/cryptos/normalized/highest")
                            .queryParam("date", "xxx")
                            .build())
                    .exchange()
                    .expectStatus()
                    .isBadRequest()
                    .expectHeader().valueEquals("X-Rate-Limit-Remaining", --attemptsLeft)
                    .expectBody();
        }

        webTestClient.get().uri(uriBuilder -> uriBuilder
                        .path("/v1/cryptos/normalized/highest")
                        .queryParam("date", "xxx")
                        .build())
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.TOO_MANY_REQUESTS);
    }
}
