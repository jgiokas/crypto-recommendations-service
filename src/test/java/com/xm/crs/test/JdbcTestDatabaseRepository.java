package com.xm.crs.test;

import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import com.xm.crs.persitence.CryptoRecommendationRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcTestDatabaseRepository {

    private final CryptoRecommendationRepository cryptoRecommendationRepository;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcTestDatabaseRepository(CryptoRecommendationRepository cryptoRecommendationRepository, NamedParameterJdbcTemplate jdbcTemplate) {
        this.cryptoRecommendationRepository = cryptoRecommendationRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void saveAll(List<CryptoMarketValue> cryptoMarketValues) {
        cryptoRecommendationRepository.saveAll(cryptoMarketValues);
    }

    public void truncate() {
        StringBuilder sb = new StringBuilder("TRUNCATE ");
        for (int i = 0; i < Cryptocurrency.values().length; i++) {
            final var table = Cryptocurrency.values()[i].name().toLowerCase();
            sb.append(table);
            if (i < Cryptocurrency.values().length - 1) {
                sb.append(", ");
            }
        }
        jdbcTemplate.update(sb.toString(), new MapSqlParameterSource());
    }
}
