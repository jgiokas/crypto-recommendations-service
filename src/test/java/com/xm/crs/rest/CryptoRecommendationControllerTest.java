package com.xm.crs.rest;

import com.xm.crs.BaseTest;
import com.xm.crs.core.CryptoMarketValue;
import com.xm.crs.core.Cryptocurrency;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class CryptoRecommendationControllerTest extends BaseTest {

    @AfterEach
    void cleanup() {
        jdbcTestDatabaseRepository.truncate();
    }

    @Test
    void testGetSummaryEndpoint() {

        final var expectedMinPrice = new BigDecimal("12345.67");
        final var expectedMaxPrice = new BigDecimal("98765.43");
        final var expectedEarliestPrice = new BigDecimal("23456.78");
        final var expectedLatestPrice = new BigDecimal("34567.89");
        final var otherPrice = new BigDecimal("45678.9");

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(Instant.now(), Cryptocurrency.BTC, expectedEarliestPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(1)), Cryptocurrency.BTC, expectedMinPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(2)), Cryptocurrency.BTC, expectedMaxPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(3)), Cryptocurrency.BTC, otherPrice),
                        new CryptoMarketValue(Instant.now().plus(Duration.ofHours(4)), Cryptocurrency.BTC, expectedLatestPrice)
                )
        );

        webTestClient.get().uri("/v1/cryptos/{crypto}", "BTC")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.minPrice").isEqualTo(expectedMinPrice)
                .jsonPath("$.maxPrice").isEqualTo(expectedMaxPrice)
                .jsonPath("$.earliestPrice").isEqualTo(expectedEarliestPrice)
                .jsonPath("$.latestPrice").isEqualTo(expectedLatestPrice);
    }

    @Test
    void testGetSummaryEndpointInvalidPathVariable() {
        webTestClient.get().uri("/v1/cryptos/{crypto}", "xxx")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error").isEqualTo("Invalid or unsupported cryptocurrency: xxx")
                .jsonPath("$.status").isEqualTo(400)
                .jsonPath("$.timestamp").isNotEmpty();
    }

    @Test
    void testGetHighestNormalizedRangeEndpoint() {
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(todayInstant, Cryptocurrency.BTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.BTC, new BigDecimal("2.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.ETH, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.ETH, new BigDecimal("3.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.XRP, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.XRP, new BigDecimal("4.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.LTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.LTC, new BigDecimal("5.00")),
                        new CryptoMarketValue(todayInstant, Cryptocurrency.DOGE, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofHours(1)), Cryptocurrency.DOGE, new BigDecimal("6.00"))
                )
        );

        final var date = todayInstant.atZone(ZoneOffset.UTC).toLocalDate().format(DateTimeFormatter.ISO_LOCAL_DATE);

        webTestClient.get().uri(uriBuilder -> uriBuilder
                        .path("/v1/cryptos/normalized/highest")
                        .queryParam("date", date)
                        .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.symbol").isEqualTo("DOGE")
                .jsonPath("$.normalizedRange").isEqualTo(new BigDecimal("5.0"));
    }

    @Test
    void testGetHighestNormalizedRangeEndpointInvalidParameter() {
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);

        webTestClient.get().uri(uriBuilder -> uriBuilder
                        .path("/v1/cryptos/normalized/highest")
                        .queryParam("date", "xxx")
                        .build())
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("$.error").isEqualTo("Invalid date: xxx")
                .jsonPath("$.status").isEqualTo(400)
                .jsonPath("$.timestamp").isNotEmpty();
    }

    @Test
    void testGetHighestNormalizedRangeEndpointNoResults() {
        webTestClient.get().uri(uriBuilder -> uriBuilder
                        .path("/v1/cryptos/normalized/highest")
                        .queryParam("date", "1970-01-01")
                        .build())
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.error").isEqualTo("No results found in given date: 1970-01-01")
                .jsonPath("$.status").isEqualTo(404)
                .jsonPath("$.timestamp").isNotEmpty();
    }

    @Test
    void testGetAllNormalizedEndpoint() {
        // given
        final var todayInstant = Instant.now().truncatedTo(ChronoUnit.DAYS);

        jdbcTestDatabaseRepository.saveAll(
                List.of(
                        new CryptoMarketValue(todayInstant, Cryptocurrency.BTC, new BigDecimal("1.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.BTC, new BigDecimal("2.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.BTC, new BigDecimal("3.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.ETH, new BigDecimal("4.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.ETH, new BigDecimal("5.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.ETH, new BigDecimal("7.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.XRP, new BigDecimal("8.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.XRP, new BigDecimal("9.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.XRP, new BigDecimal("12.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.LTC, new BigDecimal("13.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.LTC, new BigDecimal("14.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.LTC, new BigDecimal("18.00")),

                        new CryptoMarketValue(todayInstant, Cryptocurrency.DOGE, new BigDecimal("19.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(1)), Cryptocurrency.DOGE, new BigDecimal("20.00")),
                        new CryptoMarketValue(todayInstant.plus(Duration.ofDays(3)), Cryptocurrency.DOGE, new BigDecimal("25.00"))
                )
        );

        webTestClient.get().uri("/v1/cryptos/normalized")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$").isNotEmpty();
    }


}
